﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repostajes
{
    public class Matricula
    {
        public string MatriculaId { get; set; }
        public string Empresa { get; set; }
        public string TipoVehiculo { get; set; }
    }
}
