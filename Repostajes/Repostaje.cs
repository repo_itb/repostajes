﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repostajes
{
    public class Repostaje
    {
        public string YYYYTransactionID { get; set; }
        public string FleetName { get; set; }
        public string IdEstacion { get; set; }
        public string NombreEstación { get; set; }
        public string Bomba { get; set; }
        public string ReceiptPlate { get; set; }
        public string DeviceName { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string VolumeQuantity { get; set; }
        public string Odometro { get; set; }
        public string CodigoProducto { get; set; }
        public string TipoVehiculo { get; set; }
        public string NombreArchivo { get; set; }
    }
}
