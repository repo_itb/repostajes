﻿namespace Repostajes
{
    partial class Repostajes
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Repostajes));
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblProcesar = new System.Windows.Forms.Label();
            this.lblDestino = new System.Windows.Forms.Label();
            this.tbArchivosProcesar = new System.Windows.Forms.TextBox();
            this.tbDestino = new System.Windows.Forms.TextBox();
            this.lblMensajes = new System.Windows.Forms.Label();
            this.tbSistema = new System.Windows.Forms.TextBox();
            this.btnProcesar = new System.Windows.Forms.Button();
            this.btnRutaDesde = new System.Windows.Forms.Button();
            this.btnRutaHasta = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uNIMATRICULASBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.unificadaDataSet1 = new UnificadaDataSet1();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tbEmpresa = new System.Windows.Forms.TextBox();
            this.tbTipoVehiculo = new System.Windows.Forms.TextBox();
            this.tbMatricula = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uNI_MATRICULASTableAdapter = new UnificadaDataSet1TableAdapters.UNI_MATRICULASTableAdapter();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnDeshacer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNIMATRICULASBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unificadaDataSet1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            this.bgw.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgw_DoWork);
            this.bgw.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgw_ProgressChanged);
            this.bgw.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgw_RunWorkerCompleted);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblProcesar
            // 
            this.lblProcesar.AutoSize = true;
            this.lblProcesar.Location = new System.Drawing.Point(12, 16);
            this.lblProcesar.Name = "lblProcesar";
            this.lblProcesar.Size = new System.Drawing.Size(101, 13);
            this.lblProcesar.TabIndex = 0;
            this.lblProcesar.Text = "Archivos a procesar";
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(12, 40);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(82, 13);
            this.lblDestino.TabIndex = 1;
            this.lblDestino.Text = "Ruta de destino";
            // 
            // tbArchivosProcesar
            // 
            this.tbArchivosProcesar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbArchivosProcesar.Location = new System.Drawing.Point(120, 13);
            this.tbArchivosProcesar.Name = "tbArchivosProcesar";
            this.tbArchivosProcesar.ReadOnly = true;
            this.tbArchivosProcesar.Size = new System.Drawing.Size(502, 20);
            this.tbArchivosProcesar.TabIndex = 2;
            // 
            // tbDestino
            // 
            this.tbDestino.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDestino.Location = new System.Drawing.Point(120, 37);
            this.tbDestino.Name = "tbDestino";
            this.tbDestino.ReadOnly = true;
            this.tbDestino.Size = new System.Drawing.Size(502, 20);
            this.tbDestino.TabIndex = 3;
            // 
            // lblMensajes
            // 
            this.lblMensajes.AutoSize = true;
            this.lblMensajes.Location = new System.Drawing.Point(12, 65);
            this.lblMensajes.Name = "lblMensajes";
            this.lblMensajes.Size = new System.Drawing.Size(107, 13);
            this.lblMensajes.TabIndex = 4;
            this.lblMensajes.Text = "Mensajes del sistema";
            // 
            // tbSistema
            // 
            this.tbSistema.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSistema.Location = new System.Drawing.Point(120, 62);
            this.tbSistema.Multiline = true;
            this.tbSistema.Name = "tbSistema";
            this.tbSistema.ReadOnly = true;
            this.tbSistema.Size = new System.Drawing.Size(502, 387);
            this.tbSistema.TabIndex = 5;
            // 
            // btnProcesar
            // 
            this.btnProcesar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcesar.Location = new System.Drawing.Point(547, 460);
            this.btnProcesar.Name = "btnProcesar";
            this.btnProcesar.Size = new System.Drawing.Size(75, 23);
            this.btnProcesar.TabIndex = 6;
            this.btnProcesar.Text = "Procesar";
            this.btnProcesar.UseVisualStyleBackColor = true;
            this.btnProcesar.Click += new System.EventHandler(this.btnProcesar_Click);
            // 
            // btnRutaDesde
            // 
            this.btnRutaDesde.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRutaDesde.Location = new System.Drawing.Point(628, 12);
            this.btnRutaDesde.Name = "btnRutaDesde";
            this.btnRutaDesde.Size = new System.Drawing.Size(35, 21);
            this.btnRutaDesde.TabIndex = 9;
            this.btnRutaDesde.Text = "...";
            this.btnRutaDesde.UseVisualStyleBackColor = true;
            this.btnRutaDesde.Click += new System.EventHandler(this.btnRutaDesde_Click);
            // 
            // btnRutaHasta
            // 
            this.btnRutaHasta.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRutaHasta.Location = new System.Drawing.Point(628, 37);
            this.btnRutaHasta.Name = "btnRutaHasta";
            this.btnRutaHasta.Size = new System.Drawing.Size(35, 20);
            this.btnRutaHasta.TabIndex = 10;
            this.btnRutaHasta.Text = "...";
            this.btnRutaHasta.UseVisualStyleBackColor = true;
            this.btnRutaHasta.Click += new System.EventHandler(this.btnRutaHasta_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dataGridView1.DataSource = this.uNIMATRICULASBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(672, 80);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(373, 363);
            this.dataGridView1.TabIndex = 12;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Matricula";
            this.dataGridViewTextBoxColumn1.HeaderText = "Matricula";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TipoVehiculo";
            this.dataGridViewTextBoxColumn2.HeaderText = "TipoVehiculo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Empresa";
            this.dataGridViewTextBoxColumn3.HeaderText = "Empresa";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // uNIMATRICULASBindingSource
            // 
            this.uNIMATRICULASBindingSource.DataMember = "UNI_MATRICULAS";
            this.uNIMATRICULASBindingSource.DataSource = this.unificadaDataSet1;
            // 
            // unificadaDataSet1
            // 
            this.unificadaDataSet1.DataSetName = "UnificadaDataSet1";
            this.unificadaDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.tbEmpresa);
            this.panel1.Controls.Add(this.tbTipoVehiculo);
            this.panel1.Controls.Add(this.tbMatricula);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(672, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(371, 56);
            this.panel1.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(240, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(124, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "BUSCAR Matrícula";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbEmpresa
            // 
            this.tbEmpresa.Location = new System.Drawing.Point(264, 7);
            this.tbEmpresa.Name = "tbEmpresa";
            this.tbEmpresa.Size = new System.Drawing.Size(100, 20);
            this.tbEmpresa.TabIndex = 5;
            // 
            // tbTipoVehiculo
            // 
            this.tbTipoVehiculo.Location = new System.Drawing.Point(108, 31);
            this.tbTipoVehiculo.Name = "tbTipoVehiculo";
            this.tbTipoVehiculo.Size = new System.Drawing.Size(100, 20);
            this.tbTipoVehiculo.TabIndex = 4;
            // 
            // tbMatricula
            // 
            this.tbMatricula.Location = new System.Drawing.Point(69, 7);
            this.tbMatricula.Name = "tbMatricula";
            this.tbMatricula.Size = new System.Drawing.Size(100, 20);
            this.tbMatricula.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Empresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tipo de Vehículo";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Matricula";
            // 
            // uNI_MATRICULASTableAdapter
            // 
            this.uNI_MATRICULASTableAdapter.ClearBeforeFill = true;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(970, 460);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 14;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnDeshacer
            // 
            this.btnDeshacer.Location = new System.Drawing.Point(889, 460);
            this.btnDeshacer.Name = "btnDeshacer";
            this.btnDeshacer.Size = new System.Drawing.Size(75, 23);
            this.btnDeshacer.TabIndex = 15;
            this.btnDeshacer.Text = "Deshacer";
            this.btnDeshacer.UseVisualStyleBackColor = true;
            this.btnDeshacer.Click += new System.EventHandler(this.btnDeshacer_Click);
            // 
            // Repostajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 495);
            this.Controls.Add(this.btnDeshacer);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnRutaHasta);
            this.Controls.Add(this.btnRutaDesde);
            this.Controls.Add(this.btnProcesar);
            this.Controls.Add(this.tbSistema);
            this.Controls.Add(this.lblMensajes);
            this.Controls.Add(this.tbDestino);
            this.Controls.Add(this.tbArchivosProcesar);
            this.Controls.Add(this.lblDestino);
            this.Controls.Add(this.lblProcesar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Repostajes";
            this.Text = "Repostajes";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uNIMATRICULASBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unificadaDataSet1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label lblProcesar;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.TextBox tbArchivosProcesar;
        private System.Windows.Forms.TextBox tbDestino;
        private System.Windows.Forms.Label lblMensajes;
        private System.Windows.Forms.TextBox tbSistema;
        private System.Windows.Forms.Button btnProcesar;
        private System.Windows.Forms.Button btnRutaDesde;
        private System.Windows.Forms.Button btnRutaHasta;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn matriculaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoVehiculoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn empresaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbEmpresa;
        private System.Windows.Forms.TextBox tbTipoVehiculo;
        private System.Windows.Forms.TextBox tbMatricula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private UnificadaDataSet1 unificadaDataSet1;
        private System.Windows.Forms.BindingSource uNIMATRICULASBindingSource;
        private UnificadaDataSet1TableAdapters.UNI_MATRICULASTableAdapter uNI_MATRICULASTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnDeshacer;
    }
}

