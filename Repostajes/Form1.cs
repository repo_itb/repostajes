﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Repostajes
{
    public partial class Repostajes : Form
    {
        string[] files;
        List<Repostaje> Repostajesl = new List<Repostaje>();
        List<Matricula> Matriculas = new List<Matricula>();

        public Repostajes()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.uNI_MATRICULASTableAdapter.Fill(this.unificadaDataSet1.UNI_MATRICULAS);
            InitializeOpenFileDialog();
        }

        #region Carga Rutas Archivos

        private void InitializeOpenFileDialog()
        {
            this.openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            this.openFileDialog1.Multiselect = true;
            this.openFileDialog1.Title = "Seleccione los archivos";
        }

        private void btnRutaDesde_Click(object sender, EventArgs e)
        {
            DialogResult dr = this.openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                files = openFileDialog1.FileNames;
                foreach (String file in openFileDialog1.FileNames)
                    tbArchivosProcesar.Text += "\"" + Path.GetFileName(file) + "\", ";
            }
        }

        private void btnRutaHasta_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDlg = new FolderBrowserDialog();
            folderBrowserDlg.ShowNewFolderButton = true;
            DialogResult dlgResult = folderBrowserDlg.ShowDialog();
            if (dlgResult.Equals(DialogResult.OK))
            {
                tbDestino.Text = folderBrowserDlg.SelectedPath;
                Environment.SpecialFolder rootFolder = folderBrowserDlg.RootFolder;
            }
        }

        #endregion

        #region Habilitacion de controles

        private void DesabilitaControles()
        {
            btnProcesar.Enabled = false;
            btnRutaDesde.Enabled = false;
            btnRutaHasta.Enabled = false;
            button1.Enabled = false;
        }

        private void HabilitaControles()
        {
            btnProcesar.Enabled = true;
            btnRutaDesde.Enabled = true;
            btnRutaHasta.Enabled = true;
            button1.Enabled = true;
        }

        #endregion

        #region Proceso Principal

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidaDatos()) return;

                //Limpia formulario
                tbSistema.Text = String.Empty;
                DesabilitaControles();

                //Limpia variables
                Matriculas.Clear();
                Repostajesl.Clear();

                //Carga archivos
                MatriculasToList();
                ProcesaArchivos();

                //Comprueba matriculas
                List<Repostaje> l = CompruebaMatriculas();

                if (l.Count > 0)
                {
                    //Si Falta alguna se muestran y sale
                    tbSistema.AppendText("Faltan las siguientes matrículas en el sistema: " + Environment.NewLine);
                    foreach (var r in l.Select(x => x.ReceiptPlate).Distinct().ToList())
                        tbSistema.AppendText("- " + r + Environment.NewLine);
                }
                else
                {
                    //Si no faltan se generan los ficheros por empresa / Tipo vehiculo
                    GeneraFicheros();
                }

            }
            catch (Exception ex)
            {
                tbSistema.AppendText("******************* ERROR *******************" + Environment.NewLine);
                tbSistema.AppendText(ex.Message + Environment.NewLine);
            }
            //Formulario habilitado de nuevo
            HabilitaControles();

        }

        private bool ValidaDatos()
        {
            if (String.IsNullOrEmpty(tbArchivosProcesar.Text) || String.IsNullOrEmpty(tbDestino.Text))
            {
                MessageBox.Show("Seleccione los archivos y la ruta de destino.");
                return false;
            }
            return true;
        }

        #endregion

        #region Genera Ficheros

        private void GeneraFicheros()
        {
            List<string> archivos = Repostajesl.Select(x => x.NombreArchivo).Distinct().ToList();
            List<string> empresas = Repostajesl.Select(x => x.FleetName).Distinct().ToList();
            List<string> tipos = Repostajesl.Select(x => x.TipoVehiculo).Distinct().ToList();

            foreach (string a in archivos)
                foreach (string e in empresas)
                    foreach (string t in tipos)
                    {
                        List<Repostaje> repos = Repostajesl.Where(x => x.NombreArchivo == a && x.FleetName == e && x.TipoVehiculo == t).ToList();
                        if(repos != null && repos.Count > 0) GeneraFicheroEmpresaTipo(repos);
                    }
        }

        private void GeneraFicheroEmpresaTipo(List<Repostaje> repos)
        {
            // Construir nombre de archivo
            string name = DateTime.Now.ToString("yyyyMMddhhmmss") + repos[0].NombreArchivo + repos[0].FleetName + "_" + repos[0].TipoVehiculo;

            // Abrir archivo

            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(tbDestino.Text + "\\" + name + ".txt" ))
            {
                // Escribir cabeceras 
                file.WriteLine("YYYY+Transaction ID;Fleet name;ID de la Estación;Nombre de la estación;Bomba;Receipt plate;Device name;Transaction date;Transaction time;Volume/quantity;Odómetro;Codigo de producto");
                
                // Escribir Datos
                foreach (Repostaje r in repos)
                {
                    string linea = r.YYYYTransactionID + ";" + r.FleetName + ";" + r.IdEstacion + ";" + r.NombreEstación + ";" + r.Bomba + ";" + r.ReceiptPlate + ";" + r.DeviceName + ";" + r.TransactionDate + ";" + r.TransactionTime + ";" + r.VolumeQuantity + ";" + r.Odometro + ";" + r.CodigoProducto;
                    file.WriteLine(linea);
                }
            }
            
        }

        #endregion

        #region Comprobaciones

        private List<Repostaje> CompruebaMatriculas()
        {
            return Repostajesl.Where(x => !Matriculas.Any(a => a.MatriculaId == x.ReceiptPlate)).ToList();
        }

        #endregion

        #region Carga Repostajes

        private string LimpiaMatricula(string m)
        {
            return m.Trim().Replace(" ", "").Replace("-", "");
        }

        private void ProcesaArchivos()
        {
            foreach (string file in files)
                Repostajesl.AddRange(ProcesaArchivo(file));

        }

        private List<Repostaje> ProcesaArchivo(string file)
        {
            List<Repostaje> lt = new List<Repostaje>();
            string[] readText = null;
            if (File.Exists(file))
                readText = File.ReadAllLines(file);
            string name = Path.GetFileNameWithoutExtension(file);
            lt = readText.Select(x => StringToRepostaje(x, name)).ToList();

            lt.RemoveAt(0);

            return lt;
        }

        private Repostaje StringToRepostaje(string t, string n)
        {
            string[] tt = t.Split(';');

            Matricula m = Matriculas.FirstOrDefault(x => x.MatriculaId == LimpiaMatricula(tt[5]));

            string tipo = "";
            if (m != null)
                tipo = m.TipoVehiculo;

            Repostaje lt = new Repostaje()
            {
                YYYYTransactionID = tt[0].Trim(),
                FleetName = tt[1].Trim(),
                IdEstacion = tt[2].Trim(),
                NombreEstación = tt[3].Trim(),
                Bomba = tt[4].Trim(),
                ReceiptPlate = LimpiaMatricula(tt[5]),
                DeviceName = tt[6].Trim(),
                TransactionDate = tt[7].Trim(),
                TransactionTime = tt[8].Trim(),
                VolumeQuantity = tt[9].Trim(),
                Odometro = tt[10].Trim(),
                CodigoProducto = tt[11].Trim(),
                NombreArchivo = n,
                TipoVehiculo = tipo
            };

            return lt;
        }

        #endregion 

        #region bgw

        private void bgw_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        #endregion

        #region Matriculas

        private void button1_Click(object sender, EventArgs e)
        {
            this.uNI_MATRICULASTableAdapter.FillBy(this.unificadaDataSet1.UNI_MATRICULAS, '%' + tbMatricula.Text + '%', '%' + tbTipoVehiculo.Text + '%', '%' + tbEmpresa.Text + '%');

        }

        private void MatriculasToList()
        {
            Matriculas = unificadaDataSet1.Tables[0].AsEnumerable().Select(dataRow => new Matricula
            {
                MatriculaId = dataRow.Field<string>("Matricula").Trim(),
                TipoVehiculo = dataRow.Field<string>("TipoVehiculo").Trim(),
                Empresa = dataRow.Field<string>("Empresa").Trim()
            }).ToList();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();

                this.uNIMATRICULASBindingSource.EndEdit();
                this.uNI_MATRICULASTableAdapter.Update(this.unificadaDataSet1.UNI_MATRICULAS);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }

        private void btnDeshacer_Click(object sender, EventArgs e)
        {
            this.uNI_MATRICULASTableAdapter.Fill(this.unificadaDataSet1.UNI_MATRICULAS);
        }

        #endregion

    }
}
